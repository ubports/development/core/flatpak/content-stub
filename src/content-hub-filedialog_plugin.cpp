// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-only

#include "content-hub-filedialog_plugin.h"

#include "contenthandler.h"
#include "contenthub.h"
#include "contentitem.h"
#include "contentpeer.h"
#include "contentpeermodel.h"
#include "contentscope.h"
#include "contentstore.h"
#include "contenttransfer.h"

#include <QQmlEngine>

void ContentHubFiledialogPlugin::registerTypes(const char *uri)
{
    Q_INIT_RESOURCE(contentstub);

    // @uri Ubuntu.Content
    for (int major = 0; major <= 1; major++) {
        for (int minor = 0; minor <= 3; minor++) {
            qmlRegisterType<ContentHandler>(uri, major, minor, "ContentHandler");
            qmlRegisterType<ContentHub>(uri, major, minor, "ContentHub");
            qmlRegisterType<ContentItem>(uri, major, minor, "ContentItem");
            qmlRegisterUncreatableType<ContentPeer>(uri, major, minor, "ContentPeer", "abstract");
            qmlRegisterType<ContentPeerModel>(uri, major, minor, "ContentPeerModel");
            qmlRegisterType(QUrl(QStringLiteral("qrc:/contentstub/ContentPeerPicker.qml")), uri, major, minor, "ContentPeerPicker");
            qmlRegisterType<ContentScope>(uri, major, minor, "ContentScope");
            qmlRegisterType<ContentStore>(uri, major, minor, "ContentStore");
            qmlRegisterType<ContentTransfer>(uri, major, minor, "ContentTransfer");
            qmlRegisterType(QUrl(QStringLiteral("qrc:/contentstub/ContentTransferHint.qml")), uri, major, minor, "ContentTransferHint");
            qmlRegisterType<ContentType>(uri, major, minor, "ContentType");
        }
    }
}
