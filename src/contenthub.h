// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-only

#ifndef CONTENTHUB_H
#define CONTENTHUB_H

#include <QQuickItem>
#include "contenttransfer.h"

class ContentHub : public QObject
{
    Q_OBJECT

public:
    explicit ContentHub(QObject *parent = nullptr);

    static ContentHub &instance();

Q_SIGNALS:
    void exportRequested(ContentTransfer *contentTransfer);
    void importRequested(ContentTransfer *contentTransfer);
    void shareRequested(ContentTransfer *transferTransfer);
};

#endif // CONTENTHUB_H
