// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-only

#ifndef CONTENTITEM_H
#define CONTENTITEM_H

#include <QObject>
#include <QUrl>

class ContentItem : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString text MEMBER m_text)
    Q_PROPERTY(QUrl url READ url WRITE setUrl NOTIFY urlChanged)

public:
    explicit ContentItem(QObject *parent = nullptr);
    explicit ContentItem(const QUrl &url, QObject *parent = nullptr);

    Q_INVOKABLE bool move(const QUrl &dir, const QString &fileName);
    Q_INVOKABLE bool move(const QUrl &dir);
    Q_INVOKABLE QUrl toDataURI();

    QUrl url() const;
    void setUrl(const QUrl &url);

Q_SIGNALS:
    void urlChanged();

private:
    QString m_text;
    QUrl m_url;
};

#endif // CONTENTITEM_H
