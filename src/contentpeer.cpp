// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-only

#include "contentpeer.h"

#include <QFileDialog>
#include <QDesktopServices>
#include <QDebug>

#include "contentitem.h"
#include "contenthub.h"
#include "contentstore.h"

ContentPeer::ContentPeer(QObject *parent)
    : QObject(parent)
    , m_contentType(ContentType::Undefined)
    , m_contentHandler(ContentHandler::Source)
    , m_selectionType(ContentTransfer::Multiple)
{
}

ContentTransfer *FilesystemPeer::request(ContentStore *store)
{
    store->StorageLocation();
    ContentTransfer *transfer = request();
    const auto items = transfer->items();
    std::for_each(items.begin(), items.end(), [=](ContentItem *item) {
        if (item->url().isValid() && item->url().isLocalFile()) {
            const QString targetLocation = store->StorageLocation() + QDir::separator() + item->url().fileName();
            if (!QFile::copy(item->url().toLocalFile(), targetLocation)) {
                qWarning() << "Failed to copy file to ContentStore:" << targetLocation;
                return;
            }
            item->setUrl(QUrl(targetLocation));
        }
    });

    return transfer;
}

ContentTransfer *FilesystemPeer::request()
{
    delete m_fileDialog;
    m_fileDialog = nullptr;

    m_fileDialog = new QFileDialog();
    auto *transfer = new ContentTransfer(this);

    connect(m_fileDialog, &QFileDialog::rejected, this, [=] {
        transfer->setState(ContentTransfer::Aborted);
    });

    // source
    if (contentHandler() == ContentHandler::Source) {
        Q_EMIT ContentHub::instance().importRequested(transfer);

        m_fileDialog->setAcceptMode(QFileDialog::AcceptOpen);

        // single
        if (selectionType() == ContentTransfer::Single) {
            m_fileDialog->setFileMode(QFileDialog::ExistingFile);
        // multiple
        } else {
            m_fileDialog->setFileMode(QFileDialog::ExistingFiles);
        }

        transfer->setState(ContentTransfer::Initiated);
        connect(m_fileDialog, &QFileDialog::accepted, this, [=] {
            transfer->setState(ContentTransfer::InProgress);

            QList<ContentItem *> transferItems;
            const auto selectedUrls = m_fileDialog->selectedUrls();
            std::transform(selectedUrls.begin(), selectedUrls.end(), std::back_inserter(transferItems), [=](const QUrl &url) {
                return new ContentItem(std::move(url), this);
            });
            transfer->setItems(std::move(transferItems));
            transfer->setState(ContentTransfer::Charged);
            transfer->finalize();
        });
    // destination
    } else {
        transfer->setState(ContentTransfer::InProgress);
        m_fileDialog->setAcceptMode(QFileDialog::AcceptSave);
        m_fileDialog->setFileMode(QFileDialog::AnyFile);

        connect(transfer, &ContentTransfer::stateChanged, this, [=]() {
            if (transfer->state() == ContentTransfer::Charged) {
                const auto items = transfer->items();
                if (!items.empty()) {
                    const QString source = items.first()->url().toLocalFile();
                    connect(m_fileDialog, &QFileDialog::accepted, m_fileDialog, [=, dialog=m_fileDialog] {
                        if (!items.empty() && !dialog->selectedFiles().empty()) {
                            const QString destination = dialog->selectedFiles().first();
                            QFile::copy(source, destination);
                        }
                    });
                }
            }
        });
    }

    m_fileDialog->setMimeTypeFilters(contentTypeToMime(contentType()));

    m_fileDialog->open();

    return transfer;
}

QString ContentPeer::name() const
{
    return m_name;
}

void ContentPeer::setName(const QString &name)
{
    m_name = name;
    Q_EMIT nameChanged();
}

ContentType::Type ContentPeer::contentType() const
{
    return m_contentType;
}

void ContentPeer::setContentType(const ContentType::Type &contentType)
{
    m_contentType = contentType;
    Q_EMIT contentTypeChanged();
}

ContentHandler::Handler ContentPeer::contentHandler() const
{
    return m_contentHandler;
}

void ContentPeer::setContentHandler(const ContentHandler::Handler &contentHandler)
{
    m_contentHandler = contentHandler;
    Q_EMIT contentHandlerChanged();
}

ContentTransfer::SelectionType ContentPeer::selectionType() const
{
    return m_selectionType;
}

void ContentPeer::setSelectionType(const ContentTransfer::SelectionType &selectionType)
{
    m_selectionType = selectionType;
    Q_EMIT selectionTypeChanged();
}

QStringList ContentPeer::contentTypeToMime(ContentType::Type type)
{
    switch (type) {
    case ContentType::Pictures:
        return {"image/jpeg", "image/png"};
    case ContentType::Documents:
        return {
            "application/pdf",
            "application/vnd.oasis.opendocument.text",
            "application/vnd.oasis.opendocument.spreadsheet",
            "application/vnd.oasis.opendocument.presentation"
        };
    case ContentType::Music:
        return {
            "audio/mpeg",
            "audio/x-flac",
            "audio/flac",
            "audio/wave",
            "audio/wav",
            "audio/x-wav",
            "audio/x-pn-wav",
            "audio/webm",
            "audio/ogg",
            "application/ogg"
        };
    case ContentType::Videos:
        return {"video/mp4"};
    case ContentType::Text:
        return {"text/plain"};
    case ContentType::Contacts:
        return {"text/vcard"}; // TODO, probably needs compat layer
    case ContentType::EBooks:
    case ContentType::Links:
    case ContentType::Events:
    case ContentType::All:
    case ContentType::Undefined:
    case ContentType::Unknown:
        return {"application/octet-stream"};
    }

    return {};
}

FilesystemPeer::FilesystemPeer(QObject *parent)
    : ContentPeer(parent)
    , m_fileDialog(nullptr)
{
}

OpenWithPeer::OpenWithPeer(QObject *parent)
    : ContentPeer(parent)
{
}

ContentTransfer *OpenWithPeer::request(ContentStore *store)
{
    return nullptr;
}

ContentTransfer *OpenWithPeer::request()
{
    auto *transfer = new ContentTransfer(this);
    transfer->setState(ContentTransfer::InProgress);
    connect(transfer, &ContentTransfer::stateChanged, this, [=]() {
        if (transfer->state() == ContentTransfer::Charged) {
            for (const auto item : transfer->items()) {
                QDesktopServices::openUrl(item->url());
            }
        }
    });
    return transfer;
}
