// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-only

#ifndef CONTENTPEER_H
#define CONTENTPEER_H

#include <QObject>
#include "contenttype.h"
#include "contenthandler.h"
#include "contenttransfer.h"

class QFileDialog;

class ContentPeer : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString appId MEMBER m_appId NOTIFY appIdChanged)
    Q_PROPERTY(ContentType::Type contentType READ contentType WRITE setContentType NOTIFY contentTypeChanged)
    Q_PROPERTY(ContentHandler::Handler contentHandler READ contentHandler WRITE setContentHandler NOTIFY contentHandlerChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(ContentTransfer::SelectionType selectionType READ selectionType WRITE setSelectionType NOTIFY selectionTypeChanged)

public:
    explicit ContentPeer(QObject *parent = nullptr);

    Q_INVOKABLE virtual ContentTransfer *request(ContentStore *store) = 0;
    Q_INVOKABLE virtual ContentTransfer *request() = 0;

    QString name() const;
    void setName(const QString &name);

    ContentType::Type contentType() const;
    void setContentType(const ContentType::Type &contentType);

    ContentHandler::Handler contentHandler() const;
    void setContentHandler(const ContentHandler::Handler &contentHandler);

    ContentTransfer::SelectionType selectionType() const;
    void setSelectionType(const ContentTransfer::SelectionType &selectionType);

Q_SIGNALS:
    void appIdChanged();
    void contentTypeChanged();
    void contentHandlerChanged();
    void nameChanged();
    void selectionTypeChanged();

protected:
    static QStringList contentTypeToMime(ContentType::Type type);

private:

    QString m_appId;
    QString m_name;
    ContentType::Type m_contentType;
    ContentHandler::Handler m_contentHandler;
    ContentTransfer::SelectionType m_selectionType;

};


class FilesystemPeer : public ContentPeer
{
    Q_OBJECT

public:
    FilesystemPeer(QObject *parent = nullptr);

    Q_INVOKABLE ContentTransfer *request(ContentStore *store) override;
    Q_INVOKABLE ContentTransfer *request() override;

private:
    QFileDialog *m_fileDialog;
};


class OpenWithPeer : public ContentPeer
{
    Q_OBJECT

public:
    OpenWithPeer(QObject *parent = nullptr);

    Q_INVOKABLE ContentTransfer *request(ContentStore *store) override;
    Q_INVOKABLE ContentTransfer *request() override;
};

#endif // CONTENTPEER_H
