// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-only

#include "contentpeermodel.h"

#include "contentpeer.h"

ContentPeerModel::ContentPeerModel(QObject *parent)
    : QObject(parent)
    , m_handler(ContentHandler::Source)
    , m_contentType(ContentType::Undefined)
{
    findPeers();
}

QQmlListProperty<ContentPeer> ContentPeerModel::peers()
{
    return QQmlListProperty<ContentPeer>(this, m_peers);
}

void ContentPeerModel::findPeers()
{
    m_peers.clear();
    if (m_handler == ContentHandler::Source) {
        auto *filesystemPeer = new FilesystemPeer(this);
        filesystemPeer->setName(tr("Select File"));
        filesystemPeer->setContentType(m_contentType);
        filesystemPeer->setContentHandler(m_handler);
        m_peers.push_back(filesystemPeer);
    }

    if (m_handler == ContentHandler::Destination
        || m_handler == ContentHandler::Share) {
        auto *filesystemPeer = new FilesystemPeer(this);
        filesystemPeer->setName(tr("Save File"));
        filesystemPeer->setContentType(m_contentType);
        filesystemPeer->setContentHandler(m_handler);
        m_peers.push_back(filesystemPeer);

        auto *openWithPeer = new OpenWithPeer(this);
        openWithPeer->setName(tr("Open in App"));
        openWithPeer->setContentType(m_contentType);
        openWithPeer->setContentHandler(m_handler);
        m_peers.push_back(openWithPeer);
    }

    Q_EMIT findPeersCompleted();
    Q_EMIT peersChanged();
}

ContentType::Type ContentPeerModel::contentType() const
{
    return m_contentType;
}

void ContentPeerModel::setContentType(const ContentType::Type &contentType)
{
    m_contentType = contentType;
    findPeers();
    Q_EMIT contentTypeChanged();
}

ContentHandler::Handler ContentPeerModel::handler() const
{
    return m_handler;
}

void ContentPeerModel::setHandler(const ContentHandler::Handler &handler)
{
    m_handler = handler;
    Q_EMIT handlerChanged();
}
