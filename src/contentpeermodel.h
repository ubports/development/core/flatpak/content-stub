// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
// SPDY-FileCopyrightText: 2013 Canonical Ltd.
//
// SPDX-License-Identifier: GPL-3.0-only

#ifndef CONTENTPEERMODEL_H
#define CONTENTPEERMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include <QModelIndex>

#include "contentpeer.h"

class ContentPeerModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(ContentType::Type contentType READ contentType WRITE setContentType NOTIFY contentTypeChanged)
    Q_PROPERTY(ContentHandler::Handler handler READ handler WRITE setHandler NOTIFY handlerChanged)
    Q_PROPERTY(QQmlListProperty<ContentPeer> peers READ peers NOTIFY peersChanged)

public:
    explicit ContentPeerModel(QObject *parent = nullptr);

    QQmlListProperty<ContentPeer> peers();

    ContentHandler::Handler handler() const;
    void setHandler(const ContentHandler::Handler &handler);

    ContentType::Type contentType() const;
    void setContentType(const ContentType::Type &contentType);

Q_SIGNALS:
    void contentTypeChanged();
    void handlerChanged();
    void peersChanged();
    void findPeersCompleted();

public Q_SLOTS:
    void findPeers();

private:
    QList<ContentPeer *> m_peers;
    ContentHandler::Handler m_handler;
    ContentType::Type m_contentType;
};

#endif // CONTENTPEERMODEL_H
